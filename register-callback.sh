#!/bin/bash

export QA_PROJECT="${QA_PROJECT//\//_}"
export QA_PROJECT_SANITY="${QA_PROJECT_SANITY//\//_}"
echo "QA_SERVER: ${QA_SERVER}"
echo "QA_PROJECT: ${QA_PROJECT}"
echo "QA_PROJECT_NAME: ${QA_PROJECT_NAME}"
echo "QA_PROJECT_SANITY: ${QA_PROJECT_SANITY}"
echo "QA_PROJECT_NAME_SANITY: ${QA_PROJECT_NAME_SANITY}"
test -n "${QA_PROJECT_NAME}" || export QA_PROJECT_NAME="${QA_PROJECT}"
test -n "${QA_PROJECT_NAME_SANITY}" || export QA_PROJECT_NAME="${QA_PROJECT_SANITY}"

SQUAD_PROJECT="${QA_PROJECT=}"
REPORT_JOB_NAME="${REPORT_JOB_NAME:-'report'}"
if [ "${CI_JOB_STAGE}" = "sanity" ]; then
  SQUAD_PROJECT="${QA_PROJECT_SANITY}"
  REPORT_JOB_NAME="report-sanity"
fi

# Find report job to set up QA callback to pipeline (if CALLBACK_URL is not set)
echo "Will do tests. Need to register QA callback."
if [ -z "${CALLBACK_URL}" ]; then
  curl -f --silent "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/jobs?scope[]=manual" > jobs-manual.json
  job_id="$(jq -r ".[] | select(.name == \"${REPORT_JOB_NAME}\") | .id" jobs-manual.json)"
  CALLBACK_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/jobs/${job_id}/play"
fi
echo "Callback URL: [${CALLBACK_URL}]"

# Register callback with SQUAD
if [ -z "${BUILD_ID}" ]; then
  source lib.sh
  BUILD_ID="$(get_git_describe build.json)"
fi
registration_url="${QA_SERVER}/api/builds?version=${BUILD_ID}&project__slug=${SQUAD_PROJECT}"
echo "Registration URL: ${registration_url}"
curl -f --silent -L "${registration_url}" -o qa_build.json
build_id="$(jq -r '.results[0].id' qa_build.json)"
# If there is a json payload then add that extra parameter
cmd=(
  curl --silent
  -X POST "${QA_SERVER}/api/builds/${build_id}/callbacks/"
  -H "Authorization: Token ${QA_REPORTS_TOKEN}"
  -F "callback_url=${CALLBACK_URL}"
  -F "callback_record_response=true"
)

if [ -n "${JSON_PAYLOAD_STRING}" ]; then
  cmd+=(-F "callback_payload=${JSON_PAYLOAD_STRING}")
fi

"${cmd[@]}"
