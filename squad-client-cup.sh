#!/bin/bash

slug=${1}
name=${2}

cat << EOF | tee "run-squad-client-cmd.sh"
#!/bin/bash -e
squad-client --squad-host "${QA_SERVER}" \\
  --squad-token "${QA_REPORTS_TOKEN}" create-or-update-project \\
  --group "${QA_TEAM}" --slug "${slug}" \\
  --is-public --name "${name}" \\
  --plugins linux_log_parser,ltp \\
  --wait-before-notification-timeout "${QA_WAIT_BEFORE_NOTIFICATION_TIMEOUT}" \\
  --notification-timeout "${QA_NOTIFICATION_TIMEOUT}" \\
  --force-finishing-builds-on-timeout "${QA_FORCE_FINISHING_BUILDS_ON_TIMEOUT}" \\
  --important-metadata-keys "build-url,git_ref,git_describe,git_repo,kernel_version" \\
  --thresholds "build/*-warnings" \\
EOF

# Configure SQUAD so that it can call Gitlab using proper credentials
if [[ -v REGISTER_CALLBACK_TOKEN ]]; then
  SETTINGS="'CALLBACK_HEADERS': {'PRIVATE-TOKEN': '${REGISTER_CALLBACK_TOKEN}'}"
fi

# Configure SQUAD to be able to receive callbacks from Tuxsuite
if [[ -v TUXSUITE_PUBLIC_KEY ]]; then
  if [[ -v SETTINGS ]]; then
    SETTINGS="${SETTINGS}, "
  fi
  SETTINGS="${SETTINGS}'TUXSUITE_PUBLIC_KEY': '${TUXSUITE_PUBLIC_KEY}'"
fi

if [[ -v SETTINGS ]]; then
  cat << EOF | tee -a "run-squad-client-cmd.sh"
  --data-retention 0 \\
  --settings "{${SETTINGS}}"
EOF
else
  cat << EOF | tee -a "run-squad-client-cmd.sh"
  --data-retention 0
EOF
fi

chmod +x run-squad-client-cmd.sh
./run-squad-client-cmd.sh
