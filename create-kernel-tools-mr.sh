#!/bin/bash

set -e

tools="${1:-"kselftest"}"
extra_suitename="${2:-""}"

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
# shellcheck disable=SC1091
source "${ROOT_DIR}/lib.sh"

git config --global user.name "LKFT Bot"
git config --global user.email lkft-bot@linaro.org

clone_or_update "https://${GIT_USER}:${GIT_PASS}@gitlab.com/mrchapp/lkft-common.git" origin lkft-common

branch="autobranch-$(date -u +"%Y%m%d-%H%M%S")"
merge_date="$(date -u +"%Y-%m-%d")"

git -C lkft-common status
git -C lkft-common checkout -B "${branch}" origin/master

# Changes in lkft-common
echo "# Making changes in lkft-common..."
python3 parse-build-plan-json.py --filename build-plan.json --build-name "${tools}" | tee tmp.txt
while read -r line; do
  name=$extra_suitename$(echo "${line}" | awk -F' ' '{print $1}')
  url=$(echo "${line}" | awk -F' ' '{print $2}')
  grep -lr "$name: .*https.*" lkft-common | xargs sed -i "s|$name https.*|$name $url|g"
done < tmp.txt

echo "Committing changes in lkft-common..."
git -C lkft-common add -u .
git -C lkft-common commit -s -m "tuxconfig/plans: ${tools}: Update Tuxplan ID (${merge_date})"

echo "Creating merge-request for lkft-common..."
git -C lkft-common push \
  -o merge_request.create \
  -o merge_request.title="[auto] ${tools}: Tuxplan ID: Automerge ${merge_date}" \
  -o merge_request.target=master \
  -f origin "${branch}"

rm tmp.txt
